import json
import traci
import traci.constants as tc
from colorama import Fore

from app import Config
from app.entitiy.CarRegistry import CarRegistry
from app.logging import info
from app.network.Network import Network

from app.routing.CustomRouter import CustomRouter
from app.streaming import KafkaConnector

from app.logging import CSVLogger

from app import Util


class Simulation(object):
    """ here we run the simulation in """

    # the current tick of the simulation
    tick = 0
    stepTick = 0

    totalNumberCollisions = 0
    stepNumberCollisions = 0

    curr_variant = -1
    curr_traffic = -1

    @classmethod
    def applyFileConfig(cls):
        """ reads configs from a json and applies it at realtime to the simulation """
        try:
            config = json.load(open('./knobs.json'))
            CustomRouter.explorationPercentage = config['explorationPercentage']
            CustomRouter.averageEdgeDurationFactor = config['averageEdgeDurationFactor']
            CustomRouter.maxSpeedAndLengthFactor = config['maxSpeedAndLengthFactor']
            CustomRouter.freshnessUpdateFactor = config['freshnessUpdateFactor']
            CustomRouter.freshnessCutOffValue = config['freshnessCutOffValue']
        except:
            pass

    @classmethod
    def start(cls):
        """ start the simulation """
        info("# Start adding initial cars to the simulation", Fore.MAGENTA)
        # apply the configuration from the json file
        cls.applyFileConfig()
        CarRegistry.applyCarCounter()
        cls.loop()

    @classmethod
    # @profile
    def loop(cls):
        """ loops the simulation """



        # start listening to all cars that arrived at their target
        traci.simulation.subscribe((tc.VAR_ARRIVED_VEHICLES_IDS,))
        while 1:
            # Do one simulation step
            cls.tick += 1
            cls.stepTick += 1
            traci.simulationStep()
            # CrowdNavExt
            cls.totalNumberCollisions += traci.simulation.getCollidingVehiclesNumber()/2
            cls.stepNumberCollisions += traci.simulation.getCollidingVehiclesNumber()/2

            # CrowdNavExt
            #if (cls.tick % 1000) == 0:
            #    if CarRegistry.totalCarCounter == 600:
            #        CarRegistry.applyCarCounter(400)
            #    else:
            #        CarRegistry.applyCarCounter(600)

            # Check for removed cars and re-add them into the system
            for removedCarId in traci.simulation.getSubscriptionResults()[122]:
                CarRegistry.findById(removedCarId).setArrived(cls.tick)

            # let the cars process this step
            CarRegistry.processTick(cls.tick)

            # if we enable this we get debug information in the sumo-gui using global traveltime
            # should not be used for normal running, just for debugging
            # if (cls.tick % 10) == 0:
            #   for e in Network.routingEdges:
            # 1)     traci.edge.adaptTraveltime(e.id, 100*e.averageDuration/e.predictedDuration)
            # 2)     traci.edge.adaptTraveltime(e.id, e.averageDuration)
            # 3)     traci.edge.adaptTraveltime(e.id, (cls.tick-e.lastDurationUpdateTick)) # how old the data is

            # real time update of config if we are not in kafka mode
            if (cls.tick % 100) == 0:
                if Config.kafkaUpdates is False:
                    # json mode
                    cls.applyFileConfig()
                else:
                    # kafka mode
                    newConf = KafkaConnector.checkForNewConfiguration()
                    if newConf is not None:
                        cls.stepTick = 0
                        if "exploration_percentage" in newConf:
                            CustomRouter.explorationPercentage = newConf["exploration_percentage"]
                            print("setting victimsPercentage: " + str(newConf["exploration_percentage"]))
                            CarRegistry.totalVictimsGenerated = 0.0
                            CarRegistry.totalSmartCarsGenerated = 1.0
                        if "route_random_sigma" in newConf:
                            CustomRouter.routeRandomSigma = newConf["route_random_sigma"]
                            print("setting routeRandomSigma: " + str(newConf["route_random_sigma"]))
                        if "max_speed_and_length_factor" in newConf:
                            CustomRouter.maxSpeedAndLengthFactor = newConf["max_speed_and_length_factor"]
                            print("setting maxSpeedAndLengthFactor: " + str(newConf["max_speed_and_length_factor"]))
                        if "average_edge_duration_factor" in newConf:
                            CustomRouter.averageEdgeDurationFactor = newConf["average_edge_duration_factor"]
                            print("setting averageEdgeDurationFactor: " + str(newConf["average_edge_duration_factor"]))
                        if "freshness_update_factor" in newConf:
                            CustomRouter.freshnessUpdateFactor = newConf["freshness_update_factor"]
                            print("setting freshnessUpdateFactor: " + str(newConf["freshness_update_factor"]))
                        if "freshness_cut_off_value" in newConf:
                            CustomRouter.freshnessCutOffValue = newConf["freshness_cut_off_value"]
                            print("setting freshnessCutOffValue: " + str(newConf["freshness_cut_off_value"]))
                        if "trafficLoad" in newConf: #CrowdNavExt
                            CarRegistry.totalCarCounter = newConf["trafficLoad"]
                            print("setting totalCarCounter: " + str(newConf["trafficLoad"]))
                            CarRegistry.applyCarCounter()
                        if "variant_id" in newConf: #CrowdNavExt
                            print("variant_id: " + str(newConf["variant_id"]))
                        if ("trafficLoad" in newConf and cls.curr_traffic != newConf["trafficLoad"]) or ("variant_id" in newConf and cls.curr_variant != newConf["variant_id"]):
                            cls.curr_traffic = newConf["trafficLoad"]
                            cls.curr_variant = newConf["variant_id"]
                            CSVLogger.curr_conf = cls.curr_variant
                            CSVLogger.file_id = Util.getVariantName(cls.curr_traffic, cls.curr_variant)


            # print status update if we are not running in parallel mode
            if (cls.tick % 100) == 0 and Config.parallelMode is False:
                                #CSVLogger.logEvent("LOG",
                #                   [cls.tick, CarRegistry.stepTrips, CarRegistry.stepTripAverage, CarRegistry.stepTripOverheadAverage,
                #                    CarRegistry.stepVictimsGenerated/CarRegistry.stepSmartCarsGenerated, CarRegistry.stepComplaints,
                #                    CarRegistry.stepDropsOut, cls.stepNumberCollisions,CarRegistry.stepAcceptedSuggestions/CarRegistry.stepGivenSuggestions])

                if cls.stepTick >= Config.ticksToIgnore:
                    if cls.curr_variant == 3 or cls.curr_variant == 4 or cls.curr_variant == 5 or cls.curr_variant == 10:
                        gn = 'dis'
                    else:
                        if (0 if CarRegistry.stepGivenSuggestions == 0 else (CarRegistry.stepAcceptedSuggestions / CarRegistry.stepGivenSuggestions)) >= Config.acceptanceRateThreshold and CarRegistry.countActiveSmartCars() >= Config.smartCarPercentage:
                            gn = 'sat'
                        else:
                            gn = 'viol'

                    # print(str(Config.processID) + " -> Step:" + str(cls.tick) + "\n\t" +
                    #     "Driving cars: " + str(traci.vehicle.getIDCount()) + "/" + str(CarRegistry.totalCarCounter) + "\n\t" +
                    #     # "total AvgTripDuration: " + str(CarRegistry.totalTripAverage) + "(" + str(CarRegistry.totalTrips) + ")" + "\n\t" +
                    #     "step AvgTripDuration: " + str(CarRegistry.stepTripAverage) + "(" + str(CarRegistry.stepTrips) + ")" + "\n\t" +
                    #     # "total avgTripOverhead: " + str(CarRegistry.totalTripOverheadAverage) + "\n\t" +
                    #     "step avgTripOverhead: " + str(CarRegistry.stepTripOverheadAverage) + "\n\t" +
                    #     # "total percVict: " + str(CarRegistry.totalVictimsGenerated/CarRegistry.totalSmartCarsGenerated)+"("+str(CarRegistry.totalVictimsGenerated)+"/"+str(CarRegistry.totalSmartCarsGenerated)+")" + "\n\t" +
                    #     # "step percVict: " + str(CarRegistry.stepVictimsGenerated/CarRegistry.stepSmartCarsGenerated)+"("+str(CarRegistry.stepVictimsGenerated)+"/"+str(CarRegistry.stepSmartCarsGenerated)+")" + "\n\t" +
                    #     # "total numComplaints: " + str(CarRegistry.totalComplaints) + "\n\t" +
                    #     "step numComplaints: " + str(CarRegistry.stepComplaints) + "\n\t" +
                    #     # "total numDropsOut: " + str(CarRegistry.totalDropsOut) + "\n\t" +
                    #     "step numDropsOut: " + str(CarRegistry.stepDropsOut) + "\n\t" +
                    #     # "total numCollisions: " + str(cls.totalNumberCollisions) + "\n\t" +
                    #     "stepl numCollisions: " + str(cls.stepNumberCollisions) + "\n\t" +
                    #     # "total percAcceptance: " + str(CarRegistry.totalAcceptedSuggestions/CarRegistry.totalGivenSuggestions)+"("+str(CarRegistry.totalAcceptedSuggestions)+"/"+str(CarRegistry.totalGivenSuggestions)+")" + "\n\t" +
                    #     "step percAcceptance: " + str(CarRegistry.stepAcceptedSuggestions/CarRegistry.stepGivenSuggestions)+"("+str(CarRegistry.stepAcceptedSuggestions)+"/"+str(CarRegistry.stepGivenSuggestions)+")" + "\n\t" +
                    #     "active smart car: "+str(CarRegistry.countActiveSmartCars())
                    #     )

                    CSVLogger.logEvent("LOG",
                                   [CarRegistry.stepTripOverheadAverage <= Config.avgOverheadThreshold,
                                    cls.stepNumberCollisions <= Config.numCollisionThreshold,
                                    gn,
                                    CarRegistry.stepComplaints <= Config.compliantsThreshold,
                                    CarRegistry.stepDropsOut <= Config.dropoutThreshold,
                                    CarRegistry.stepSmartTripOverheadAverage <= Config.avgOverheadSmartThreshold], True)

                # CrowdNavExt
                # reset the variables that I want to evaluate for each time interval
                # TODO move this into a function in carregistry
                cls.stepNumberCollisions = 0
                CarRegistry.stepTrips = 0.0
                CarRegistry.stepSmartTrips = 0.0
                CarRegistry.stepTripAverage = 0.0
                CarRegistry.stepTripOverheadAverage = 0.0
                CarRegistry.stepSmartTripOverheadAverage = 0.0
                CarRegistry.stepSmartCarsGenerated = 0.0
                CarRegistry.stepVictimsGenerated = 0.0
                CarRegistry.stepComplaints = 0
                CarRegistry.stepDropsOut = 0
                CarRegistry.stepAcceptedSuggestions = 0.0
                CarRegistry.stepGivenSuggestions = 0.0

            # if we are in paralllel mode we end the simulation after 10000 ticks with a result output
            if (cls.tick % 10000) == 0 and Config.parallelMode:
                # end the simulation here
                print(str(Config.processID) + " -> Step:" + str(cls.tick) + " # Driving cars: " + str(
                    traci.vehicle.getIDCount()) + "/" + str(
                    CarRegistry.totalCarCounter) + " # avgTripDuration: " + str(
                    CarRegistry.totalTripAverage) + "(" + str(
                    CarRegistry.totalTrips) + ")" + " # avgTripOverhead: " + str(
                    CarRegistry.totalTripOverheadAverage))
                return
