#
# Config file for SUMOS
#

# should use kafka for config changes (else it uses json file)
kafkaUpdates = True

# the kafka host we want to send our messages to
kafkaHost = "localhost:9092"

# the topic we send the kafka messages to
kafkaTopic = "crowd-nav-trips"

# where we recieve system changes
kafkaCommandsTopic = "crowd-nav-commands"

# True if we want to use the SUMO GUI (always of in parallel mode)
sumoUseGUI = False  # False

# The network config (links to the net) we use for our simulation
sumoConfig = "./app/map/eichstaedt.sumo.cfg"

# The network nets to use for the simulation
# sumoNet = "./app/map/eichstaedt.normal.staticTL.lights.net.xml"
# sumoNet = "./app/map/eichstaedt.extreme.staticTL.lights.net.xml"
# sumoNet = "./app/map/eichstaedt.normal.staticTL.nolights.net.xml"
# sumoNet = "./app/map/eichstaedt.extreme.staticTL.nolights.net.xml"
# sumoNet = "./app/map/eichstaedt.normal.adaptiveTL.net.xml"
# sumoNet = "./app/map/eichstaedt.extreme.adaptiveTL.net.xml"
# sumoNet = "./app/map/eichstaedt.normal.rbl.net.xml"
sumoNet = "./app/map/eichstaedt.extreme.rbl.net.xml"

# the total number of cars we use in our simulation
totalCarCounter = 300

# percentage of cars that are smart
smartCarPercentage = 0.1

# runtime dependent variable
processID = 0
parallelMode = False

######### CrowdNavExt #########
ticksToIgnore = 500

# possible routers
ROUTER_DEFAULT = "SUMO"
ROUTER_INTERNAL = "INT_SUMO" #this is for distinguish the violations
ROUTER_ADAPTIVE = "ADAPTIVE"
ROUTER_STATIC = "STATIC"

# The thresholds for the monitoring of the objectives achievement
# Max desired average trip overhead
avgOverheadThreshold = 2.5
# Max desired number of collisions
numCollisionThreshold = 4
# Min desired suggestions acceptance probability
acceptanceRateThreshold = 0.8
# Max desired number of compliants
compliantsThreshold = 3
# Max desired number of dropouts
dropoutThreshold = 1
# Max desired average trip overhead for smart cars
avgOverheadSmartThreshold = 2.5

# Table describing the 12 possible requirement variants
variants_colnames=["N_N", "N_NP", "N_NPS", "N_NPA", "N_NN", "N_J", "N_JT", "N_JTS", "N_JTA", "N_JTP", "N_JN"]
variants = [[-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1],  #first one is ignored just for simplicity
            [1,1,1,0,1,1,1,1,0,0,1],
            [1,1,0,1,1,1,1,1,0,0,1],
            [0,0,0,0,0,1,1,1,0,0,1],
            [0,0,0,0,0,1,1,0,0,1,1],
            [0,0,0,0,0,1,0,0,0,0,1],
            [1,1,1,0,1,1,0,0,0,0,1],
            [1,1,0,1,1,1,0,0,0,0,1],
            [1,1,1,0,1,1,1,0,1,0,1],
            [1,1,0,1,1,1,1,0,1,0,1],
            [0,0,0,0,0,1,1,0,1,0,1],
            [1,1,1,0,1,1,1,0,0,1,1],
            [1,1,0,1,1,1,1,0,0,1,1]]
