""" Helper functions """
from app import Config


def addToAverage(totalCount, totalValue, newValue):
    """ simple sliding average calculation """
    return ((1.0 * totalCount * totalValue) + newValue) / (totalCount + 1)


######### CrowdNavExt #######

def getVariantName(traffic, var_id):
    context_id = 0
    if 'normal' in Config.sumoNet:
        if traffic  <= 400:
            context_id = 1
        else:
            context_id = 2
    else:
        if 'extreme' in Config.sumoNet:
            if traffic  <= 400:
                context_id = 3
            else: context_id = 4
    return 'C'+str(context_id)+'C'+str(var_id)

def getReqIndex(req, names):
    for i in range(len(names)):
        if names[i] == req:
            return i

# returns true if the requirement with id req_index is active in the current requirement variant, false otherwise
def isReqActive(req_index, var_id, variants):
    return variants[var_id][req_index]==1