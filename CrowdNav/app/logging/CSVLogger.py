import csv
import time
import os

from app import Config

csv.register_dialect('custom', doublequote=False, quoting=csv.QUOTE_NONE)
# col_names = ['tick',
#             'weather', 'day/night', 'traffic load',
#             'sourceID', 'targetID', 'durationForTrip', 'minimalCosts', 'tripOverhead', 'carID', 'isSmart', 'isVictim',
#             'complaint', 'droppedOut', 'RouteAccepted', 'RouterUsed', 'explorationPercentage']
col_names_req = ['tick', 'carID', 'C_W', 'C_T', 'N_NP', 'N_NPA', 'V_NPAP', 'N_NPS', 'N_NN', 'N_J', 'N_JT', 'N_JTA', 'N_JTS', 'N_JTP', 'N_JN']
col_names_goals = ['O_1', 'O_2', 'N_N', 'O_N1', 'O_N2', 'O_N3']

folder_id = time.strftime("%Y%m%d-%H%M%S")
folder = './data/' +folder_id + "/"
if not os.path.exists(folder):
    os.makedirs(folder)

file_id = "init"

curr_variant = -1

def logEvent(file, row, goals=False):
    try:
        file_name = folder + file_id + '.csv' #str(Config.processID)
        with open(file_name, 'ab+') as mycsvfile:
            writer = csv.writer(mycsvfile, dialect='excel')
            nr_rows = sum(1 for r in csv.reader(mycsvfile, dialect='excel'))
            if  nr_rows <= 1: #EMPTY LOG FILE, WRITE THE HEADER
                writer.writerow(col_names_req + col_names_goals)
                nr_rows = 2
            else:
                nr_rows += 1
            if goals:
                writer.writerow(([None] * len(col_names_req)) + row)
            else:
                next_row= nr_rows+1
                writer.writerow(row + ['=P'+str(next_row), '=Q'+str(next_row), '=R'+str(next_row), '=S'+str(next_row), '=T'+str(next_row), '=U'+str(next_row),])
    except Exception as e:
        print("error logging" + str(e))
