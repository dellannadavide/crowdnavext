import random
import traci
import traci.constants as tc
from app import Config

from app.Util import addToAverage
from app.logging import CSVLogger
from app.network.Network import Network
from app.routing.CustomRouter import CustomRouter
from app.routing.RouterResult import RouterResult
from app.streaming import KafkaForword


class Car:
    """ a abstract class of something that is driving around in the streets """

    def __init__(self, id):
        # the string id
        self.id = id  # type: str
        # the rounds this car already drove
        self.rounds = 0  # type: int
        # the current route as a RouterResult
        self.currentRouterResult = None  # type: RouterResult
        # when we started the route
        self.currentRouteBeginTick = None
        # the id of the current route (somu)
        self.currentRouteID = None  # type: str
        # the id of the current edge/street the car is driving (sumo)
        self.currentEdgeID = None
        # the tick this car got on this edge/street
        self.currentEdgeBeginTick = None
        # the target node this car drives to
        self.targetID = None
        # the source node this car is coming from
        self.sourceID = None
        # if it is disabled, it will stop driving
        self.disabled = False
        # the cars acceleration in the simulation
        self.acceleration = max(1, random.gauss(4, 2))
        # the cars deceleration in the simulation
        self.deceleration = max(1, random.gauss(6, 2))
        # the driver imperfection in handling the car
        self.imperfection = min(0.9, max(0.1, random.gauss(0.5, 0.5)))
        # is this car a smart car
        self.smartCar = Config.smartCarPercentage > random.random()
        ###### CrowdNavExt ##########
        # the car dropped out of crowdnav
        self.droppedOut = False
        # the number of complaints of the car, if it is a smart car
        self.numberComplaints = 0
        # list containing the expiration times of previous complaints for the agent
        self.listComplaintsExpTime = []
        # the period of time (number of ticks) a complaint is kept in memory before expiring
        self.timeComplaint = 1500
        # the max number of compliant accepted before dropping out
        self.maxNumComplaints = 3
        # threshold for overhead to trigger complaint
        self.overheadThreshold = 2.5
        # the cars likelihood of not respecting traffic rules
        self.impatience = 0
        # the type of car (in terms of the assigned routing algo): 0 adaptive, 1 Dijkstra
        self.carType = random.randint(0, 1)
        # likelihood of accepting the suggestion of the custom routing algorithm
        self.acceptSuggPerc = 0.8
        self.currRouteAccepted = True
        self.currentRouterUsed = Config.ROUTER_DEFAULT
        self.currentRouterAssigned = Config.ROUTER_DEFAULT

        self.reactionTime = 1



    def setArrived(self, tick):
        """ car arrived at its target, so we add some statistic data """

        # import here because python can not handle circular-dependencies
        from app.entitiy.CarRegistry import CarRegistry
        # add a round to the car
        self.rounds += 1

        if tick > Config.ticksToIgnore: # and self.smartCar:  # as we ignore the first 1000 ticks for this
            # add a rounte to the global registry
            CarRegistry.totalTrips += 1
            CarRegistry.stepTrips += 1
            # add the duration for this route to the global tripAverage
            durationForTip = (tick - self.currentRouteBeginTick)
            CarRegistry.totalTripAverage = addToAverage(CarRegistry.totalTrips,  # 100 for faster updates
                                                        CarRegistry.totalTripAverage,
                                                        durationForTip)
            CarRegistry.stepTripAverage = addToAverage(CarRegistry.stepTrips,  # 100 for faster updates
                                                        CarRegistry.stepTripAverage,
                                                        durationForTip)
            # CSVLogger.logEvent("arrived", [tick, self.sourceID, self.targetID,
            #                                durationForTip, self.id,self.currentRouterResult.isVictim])
            # log the overrhead values
            minimalCosts = CustomRouter.minimalRoute(self.sourceID, self.targetID, None, None).totalCost
            tripOverhead = durationForTip / minimalCosts
            CarRegistry.totalTripOverheadAverage = addToAverage(CarRegistry.totalTrips,
                                                                CarRegistry.totalTripOverheadAverage,
                                                                tripOverhead)
            CarRegistry.stepTripOverheadAverage = addToAverage(CarRegistry.stepTrips,
                                                                CarRegistry.stepTripOverheadAverage,
                                                                tripOverhead)
            if self.smartCar:
                CarRegistry.totalSmartTripOverheadAverage = addToAverage(CarRegistry.totalSmartTrips,
                                                                    CarRegistry.totalSmartTripOverheadAverage,
                                                                    tripOverhead)
                CarRegistry.stepSmartTripOverheadAverage = addToAverage(CarRegistry.stepSmartTrips,
                                                                   CarRegistry.stepSmartTripOverheadAverage,
                                                                   tripOverhead)
            # log to kafka
            msg = dict()
            msg["tick"] = tick
            msg["overhead"] = tripOverhead
            # CrowdNavExt
            # msg["victim"] = self.currentRouterResult.isVictim
            if tripOverhead > self.overheadThreshold and self.smartCar and not self.droppedOut:
                CarRegistry.totalComplaints += 1
                CarRegistry.stepComplaints += 1
                self.numberComplaints += 1
                self.listComplaintsExpTime.append(tick+self.timeComplaint)
            if self.numberComplaints > self.maxNumComplaints and self.smartCar and not self.droppedOut:
                self.droppedOut = True
                CarRegistry.totalDropsOut += 1
                CarRegistry.stepDropsOut += 1

            #CSVLogger.logEvent("LOG", [tick, 'extreme' if 'extreme' in Config.sumoNet else 'normal',
            #                           'night' if 'nolights' in Config.sumoNet else 'day',
            #                           Config.totalCarCounter,
            #                           self.sourceID, self.targetID, durationForTip, minimalCosts, tripOverhead,
            #                           self.id, self.smartCar, self.currentRouterResult.isVictim, tripOverhead > self.overheadThreshold,
            #                           self.droppedOut, self.currRouteAccepted, self.currentRouterUsed, CustomRouter.explorationPercentage])

            # notice no violations for these reqs
            # gnp: a navigation service is provided/distributed to cars
            # gnpa: an adaptive navigation service is provided/distributed to cars
            # gnps: a static navigation service is provided/distributed to cars
            #if CSVLogger.curr_variant == 1 or CSVLogger.curr_variant == 2 or CSVLogger.curr_variant == 6 or CSVLogger.curr_variant == 7 or CSVLogger.curr_variant == 8 or CSVLogger.curr_variant == 9 or CSVLogger.curr_variant == 11 or CSVLogger.curr_variant == 12:
            if CSVLogger.curr_variant == 3 or CSVLogger.curr_variant == 4 or CSVLogger.curr_variant == 5 or CSVLogger.curr_variant ==10:
                    gnp = gnps = gnpa = gnn = 'dis'
            else:
                gnp = 'sat'
                if self.smartCar:
                    if self.currentRouterAssigned == Config.ROUTER_ADAPTIVE:
                        gnpa = 'sat'
                        gnps = 'dis'
                    elif self.currentRouterAssigned == Config.ROUTER_STATIC:
                        gnps = 'sat'
                        gnpa = 'dis'
                else:
                    gnpa = 'dis'
                    gnps = 'dis'
                    gnp = 'dis'

                # gnn: smart cars shall follow the suggestion given by the navigation system
                if self.smartCar and self.currRouteAccepted:
                    gnn = 'sat'
                elif self.smartCar and not self.currRouteAccepted:
                    gnn = 'viol'
                elif not self.smartCar or self.droppedOut:
                    gnn = 'dis'

            # notice no evaluation of sat/viol implemented for these reqs
            # gj: smart cars shall follow the suggestion given by the navigation system
            # gjt: smart regulationsshall be used in appropriate junctions, for the other junctions priority rules apply
            # gjta: adaptive traffic lights shall be used
            # gjts: static traffic lights shall be used
            # gjtp: priority rules are used instead of tl
            # gjn: each car in the city shall respect the specified junctions rules

            gj = 'act'
            gjn = 'act'

            if 'rbl' in Config.sumoNet:
                gjt = gjts = gjta = gjtp ='dis'
            else:
                gjt = 'act'
                if 'actuated' in Config.sumoNet:
                    gjta = 'act'
                    gjts = 'dis'
                    gjtp = 'dis'
                elif 'static' in Config.sumoNet:
                    gjts = 'act'
                    gjta = 'dis'
                    gjtp = 'dis'
                else: #nolights, priority
                    gjts = 'dis'
                    gjta = 'dis'
                    gjtp = 'act'


            CSVLogger.logEvent("LOG", [tick, self.id,
                                       'extreme' if 'extreme' in Config.sumoNet else 'normal',
                                       'day' if CarRegistry.totalCarCounter > 400 else 'night',
                                       gnp, gnpa, CustomRouter.explorationPercentage, gnps, gnn,
                                       gj, gjt, gjta, gjts,gjtp, gjn])

            KafkaForword.publish(msg)
        # if car is still enabled, restart it in the simulation
        if self.disabled is False:
            self.addToSimulation(tick)

    def __createNewRoute(self, tick):
        """ creates a new route to a random target and uploads this route to SUMO """
        # import here because python can not handle circular-dependencies
        from app.entitiy.CarRegistry import CarRegistry
        if self.targetID is None:
            self.sourceID = randomStartNodeID = random.choice(Network.nodes).getID()
        else:
            self.sourceID = self.targetID  # We start where we stopped
        # random target
        self.targetID = random.choice(Network.nodes).getID()
        self.currentRouteID = self.id + "-" + str(self.rounds)
        # in conf 1,6,8,11 it is used a static nav service
        # in conf 2,7,9,12 it is used an adaptive nav service
        if CSVLogger.curr_variant == 2 or CSVLogger.curr_variant == 7 or CSVLogger.curr_variant == 9 or CSVLogger.curr_variant == 12:
            self.currentRouterResult = CustomRouter.route(self.sourceID, self.targetID, tick, self)
            self.currentRouterAssigned = Config.ROUTER_ADAPTIVE
        else:
            self.currentRouterResult = CustomRouter.minimalRoute(self.sourceID, self.targetID, None, None)
            #self.currentRouterResult = CustomRouter.route(self.sourceID, self.targetID, tick, self)
            self.currentRouterAssigned = Config.ROUTER_STATIC
        if len(self.currentRouterResult.route) > 0:
            traci.route.add(self.currentRouteID, self.currentRouterResult.route)
            # set color to red
            return self.currentRouteID
        else:
            # recursion aka. try again as this should work!
            return self.__createNewRoute(tick)

    def processTick(self, tick):
        """ process changes that happened in the tick to this car """
        # CrowdNavExt
        # updates the memory of previous complaints
        if len(self.listComplaintsExpTime) > 0:
            if self.listComplaintsExpTime[0] == tick:
                print("at " + str(tick) + "expires first of " + str(self.listComplaintsExpTime))
                del self.listComplaintsExpTime[0]
                self.numberComplaints -= 1
        roadID = traci.vehicle.getSubscriptionResults(self.id)[80]
        if roadID != self.currentEdgeID and self.smartCar:
            if self.currentEdgeBeginTick is not None:
                CustomRouter.applyEdgeDurationToAverage(self.currentEdgeID, tick - self.currentEdgeBeginTick, tick)
                # CSVLogger.logEvent("edge", [tick, self.currentEdgeID,
                #                             tick - self.currentEdgeBeginTick, self.id])
                # log to kafak
                # msg = dict()
                # msg["tick"] = tick
                # msg["edgeID"] = self.currentEdgeID,
                # msg["duration"] = tick - self.currentEdgeBeginTick
            # print("changed route to: " + roadID)
            self.currentEdgeBeginTick = tick
            self.currentEdgeID = roadID
            pass

    def addToSimulation(self, tick):
        """ adds this car to the simulation through the traci API """
        # import here because python can not handle circular-dependencies
        from app.entitiy.CarRegistry import CarRegistry
        self.currentRouteBeginTick = tick
        try:
            traci.vehicle.add(self.id, self.__createNewRoute(tick), tick, -4, -3)
            traci.vehicle.subscribe(self.id, (tc.VAR_ROAD_ID,))
            # ! currently disabled for performance reasons
            # traci.vehicle.setAccel(self.id, self.acceleration)
            # traci.vehicle.setDecel(self.id, self.deceleration)
            # traci.vehicle.setImperfection(self.id, self.imperfection)
            traci.vehicle.setTau(self.id, self.reactionTime)

            # CrowdNavExt
            #in variants 3,4,5,10 the entire nav service is not used

            if CSVLogger.curr_variant == 3 or CSVLogger.curr_variant == 4 or CSVLogger.curr_variant == 5 or CSVLogger.curr_variant == 10:
                self.smartCar = False #by setting this to False the only test that will be true will be the last branch, valid for dump cars

            if self.smartCar:
                self.currRouteAccepted = self.acceptSuggPerc > random.random()
            #######
            #if self.droppedOut:
            #    print(self.id + "dropped" + str(self.droppedOut))
            if not self.droppedOut and self.smartCar and self.currRouteAccepted:  # CrowdNavExt
                CarRegistry.totalAcceptedSuggestions += 1
                CarRegistry.stepAcceptedSuggestions += 1
                CarRegistry.totalGivenSuggestions += 1
                CarRegistry.stepGivenSuggestions += 1
                self.currentRouterUsed = self.currentRouterAssigned
                # set color to red
                if self.currentRouterResult.isVictim:
                    traci.vehicle.setColor(self.id, (0, 255, 0, 0))
                    CarRegistry.totalVictimsGenerated += 1
                    CarRegistry.stepVictimsGenerated += 1
                else:
                    traci.vehicle.setColor(self.id, (255, 0, 0, 0))
                CarRegistry.totalSmartCarsGenerated += 1
                CarRegistry.stepSmartCarsGenerated += 1
            else:
                if not self.droppedOut and self.smartCar:  # CrowdNavExt the car did not accept the suggestion
                    CarRegistry.totalGivenSuggestions += 1
                    CarRegistry.stepGivenSuggestions += 1
                    # TODO this is a possible place where to distinguish between different navigation services, in addition to the adaptive and sumo
                    traci.vehicle.changeTarget(self.id, self.currentRouterResult.route[-1])
                    self.currentRouterUsed = Config.ROUTER_INTERNAL
                    CarRegistry.totalSmartCarsGenerated += 1
                    CarRegistry.stepSmartCarsGenerated += 1
                else: # dump cars or smart cars that dropped out
                    # if self.droppedOut:
                        # print(self.id + "dropped out, now using sumo")
                    # dump car is using SUMO default routing, so we reroute using the same target
                    # putting the next line left == ALL SUMO ROUTING
                    traci.vehicle.changeTarget(self.id, self.currentRouterResult.route[-1])
                    self.currentRouterUsed = Config.ROUTER_DEFAULT
        except Exception as e:
            print("error adding" + str(e))
            # try recursion, as this should normally work
            # self.addToSimulation(tick)

    def remove(self):
        """" removes this car from the sumo simulation through traci """
        traci.vehicle.remove(self.id)
