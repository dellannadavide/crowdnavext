#CrowdNavExt

CrowdNavExt is a prototypical preliminary extension of [CrowdNav+RTX](https://github.com/Starofall/RTX/wiki/RTX-&-CrowdNav-Getting-Started-Guide).

CrowdNavExt extends CrowdNav+RTX in order to support the requirement model shown in the figure below.

![Banner](reqmodel.png)

The extension includes:

- the introduction of a static navigation service (Dijkstra) as an alternative to the adaptive centralized navigation service
- the introduction (via additional SUMO maps) of static and adaptive traffic lights, priority lane and right-first rules as alternative ways to regulate some of the junctions of the city
- the introduction (via aditional SUMO maps) of extreme weather conditions (achieved by reducing the speed limit in all roads by 25%
- the introduction of measurements for the objectives illustrated in the requirement model
- the instrumentation of the simulator to monitor requirements and objectives satisfaction

####Installation
Please for the installation follow the instructions reported in the original version of CrowdNav+RTX

####Execution

1. Start ZooKeeper and Kafka

        # from folder /path/to/kafka_2.11-1.0.0/
    
        # 1. start a ZooKeeper server
        bin/zookeeper-server-start.sh config/zookeeper.properties
    
        # 2. Start the Kafka server:
        bin/kafka-server-start.sh config/server.properties
    
        # 3. Create the topics (if necessary):
        bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic crowd-nav-trips
        bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic crowd-nav-commands


2. Execute CrowdNav in Background forever (after kafka is running)
        
        # from folder /path/to/CrowdNavExt/CrowdNav
        python forever.py

3. Execute RTX (after crowdnav is running)

        # from folder /path/to/CrowdNavExt/RTX
        python rtx.py start crowdnav-step
    
    N.B. so far the changes in RTX are only in the crowdnav-step definition file      

####Run different scenarios
CrowdNavExt supports different scenarios, which can executed by changing the following parameters:

- Parameters that can be changed automatically during the simulation via RTX
    - traffic load: the number of cars in the city
    - requirement variants: one of the set of requirements in the table below reported. N.B. only some of these variants can be automatically changed at runtime, some of them require also a different map (see below traffic light) 
- Parameters that can be changed manually before starting the simulation   
    - weather: extreme or normal weather (speed limit in the road reduced by 25%). Select in the Config.py file in CrowdNav the appropriate map (eichstaedt.normal or eichstaedt.extreme files)
    - traffic lights: static, adaptive, priority lane or right-first rule. These types of management for some of the traffic lights in the city can be enabled by selecting in the Config.py file in CrowdNav the appropriate map (staticTL, adaptiveTL, ... files)
  
Variant | Description											  | Requirements						 
------- | :------------------------------------------------------ |:----------------------------------
1		| Static navigation system and static traffic lights  	  |  NS, NSD, SNS, RS, J, SJ, STL, TR	 
2		| Adaptive navigation system and static traffic lights    |  NS, NSD, ANS, RS, J, SJ, STL, TR	 
3		| Only static traffic lights  							  |  J, SJ, STL, TR					 
4		| Only priority lanes signs  							  |  J, SJ, P, TR						 
5		| All panels disabled  									  |  J, TR							 
6		| Only static navigation system  						  |  NS, NSD, SNS, RS, J, TR			 
7		| Only adaptive navigation system  						  |  NS, NSD, ANS, RS, J, TR			 
8		| Static navigation system and adaptive traffic lights    |  NS, NSD, SNS, RS, J, SJ, ATL, TR	 
9		| Adaptive navigation system and adaptive traffic lights  |  NS, NSD, ANS, RS, J, SJ, ATL, TR	 
10	  	| Only adaptive traffic lights  						  |  J, SJ, ATL, TR					 
11	  	| Static navigation system and priority lanes signs  	  |  NS, NSD, SNS, RS, J, SJ, P, TR	 
12	  	| Adaptive navigation system and priority lanes signs  	  |  NS, NSD, ANS, RS, J, SJ, P, TR 
 
      
####Contact
For any information or comment do not exitate to get in touch with me.

[Davide Dell'Anna](http://davidedellanna.com/)

Personal email: [dellannadavide@gmail.com](mailto:dellannadavide@gmail.com)

Institutional email: [d.dellanna@uu.nl](mailto:d.dellanna@uu.nl)

####Related work
- Dell'Anna, Davide, Fabiano Dalpiaz, and Mehdi Dastani. "Validating goal models via Bayesian networks." In 2018 5th International Workshop on Artificial Intelligence for Requirements Engineering (AIRE), pp. 39-46. IEEE, 2018.
- Dell’Anna, Davide, Mehdi Dastani, and Fabiano Dalpiaz. "Runtime Norm Revision Using Bayesian Networks." In International Conference on Principles and Practice of Multi-Agent Systems, pp. 279-295. Springer, Cham, 2018.
       



